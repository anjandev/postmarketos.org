title: "Regression receiving calls on the Pinephone"
date: 2021-08-27
---
Modemmanager in pmaports was upgraded to 1.18rc1, and there are changes to how
call notifications from the modem are handled on the Pinephone. As a result,
some receiving calls may be missed. SMS/text messages are not affected by this.

Despite this regression, we decided to upgrade anyways because there are
improvements in MM 1.18 for the Pinephone and other devices (SDM845), and using
1.18rc1 allows us to provide further feedback to upstream Modemmanager before
the 1.18 release.

Related: [pma!2453](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2453)

